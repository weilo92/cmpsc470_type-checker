import java.util.LinkedList;

/** Description of ASTNode class
 * Abstract superclass that is used to provide
 * framework all other node classes.
 */
abstract class ASTNode {

	int linenum;
	int colnum;

	static int typeErrors = 0; // Total number of type errors found 

	static void genIndent(int indent) {
		for (int i = 1; i<=indent; i++) {
			System.out.print("\t");
		}
	} // genIndent
	static void mustBe(boolean assertion) {
		if (! assertion) {
			throw new RuntimeException();
		}
	} // mustBe
	static void typeMustBe(int testType,int requiredType,String errorMsg) {
		if ((testType != Types.Error) && (testType != requiredType)) {
			System.out.println(errorMsg);
			typeErrors++;
		}
	} // typeMustBe
	String error() {
		return "Error (line " + linenum + "): ";
	} // error

	public static SymbolTable st = new SymbolTable();
	public static SymbolInfo currentMethod;
	public static LinkedList<Kinds> kindsParam;
	public static LinkedList<Types> typesParam;


	ASTNode() {linenum=-1; colnum=-1;}
	ASTNode(int l, int c) {linenum = l;colnum=c;}
	boolean isNull() {return false;} // Is this node null?
		
	void checkTypes() {}
		// This will normally need to be redefined in a subclass
} // abstract class ASTNode

/** Description of nullNode class
 * Class used as return type for "delta" transitions, empty RHS in grammar rules.
 */
class nullNode extends ASTNode {
	nullNode() {super();}
	boolean isNull() {return true;}
}
/* End of nullNode class */

/** Description of csxLiteNode class
 * Example node class used in sample lite.csx file.
 */
class csxLiteNode extends ASTNode {

	csxLiteNode(fieldDeclsNode decls, stmtsNode stmts, int line, int col) {
		super(line,col);
		fields = decls;
		progStmts = stmts;
	} // csxLiteNode

	void checkTypes() {
		fields.checkTypes();
		progStmts.checkTypes();
	} // checkTypes
	boolean isTypeCorrect() {
		st.openScope();
		checkTypes();
		return (typeErrors == 0);
	} // isTypeCorrect

	private final stmtsNode progStmts;
	private final fieldDeclsNode fields;
} 
/* End of csxLiteNode class */

/** Description of classNode class
 * Used for following grammar rule
 * Program -> class id {Memberdecls}
 * Node used to contain contents of a class, root node
 */
class classNode extends ASTNode {
	classNode(identNode id, memberDeclsNode m, int line, int col) {
		super(line,col);
		className = id;
		members = m;
	}

	/** Description of checkTypes method
	 * Adds class name to scope, type checks nodes within
	 */			
	void checkTypes() {

		// Create new symbolInfo class and add to current scope
		SymbolInfo id = new SymbolInfo(className.idname, new Kinds(Kinds.Class), new Types(Types.Void));

		try {
			st.insert(id);
		}
		catch (DuplicateException d) {/* can't happen */} 
		catch (EmptySTException e) {/* can't happen */}

		// type check member declarations
		members.checkTypes();
	}

	/** Description of isTypeCorrect method
	 * Starts the process of type checking
	 * Returns back boolean value when done.
	 */			
	boolean isTypeCorrect() {
		st.openScope(); // open new scope
		checkTypes(); /// call type check
		return (typeErrors == 0);
	} 

	/** Description of returnErrors method
	 * Used to print number of errors at end of P4
	 */			
	int returnErrors() {
		return typeErrors;
	}
	
	private final identNode className;
	private final memberDeclsNode members;
}
/* End of classNode class */

/** Description of memberDeclsNode class
 * Used for the following grammar rules
 * Memberdecls -> Fielddecl Memberdecls
 * Memberdelcs -> Methoddecls
 * Node used to build field and method declarations inside
 * class node.
 */
class memberDeclsNode extends ASTNode {
	memberDeclsNode(fieldDeclsNode f, methodDeclsNode m, int line, int col) {
		super(line,col);
		fields = f;
		methods = m;
	} // memberDeclsNode

	/** Description of checkTypes method
	 * Runs type checking on contained field declarations node
	 * and on method declarations.
	 */		
	void checkTypes() {
		fields.checkTypes();
		methods.checkTypes();
	}

	fieldDeclsNode fields;
	private final methodDeclsNode methods;
}
/* End of memberDeclsNode class */

/** Description of fieldDeclsNode class
 * Used for the following grammar rules
 * Fielddecls -> Fielddecl Fielddecls
 * Fielddelcs -> λ
 * Node used to build field declarations
 */
class fieldDeclsNode extends ASTNode {
	fieldDeclsNode() {super();}
	fieldDeclsNode(declNode d, fieldDeclsNode f, int line, int col) {
		super(line,col);
		thisField = d;
		moreFields = f;
	}

	/** Description of checkTypes method
	 * Runs type checking on contained declaration node
	 * and on field declarations node.
	 */	
	void checkTypes() {
		thisField.checkTypes();
		moreFields.checkTypes();
	} 

	static nullFieldDeclsNode NULL = new nullFieldDeclsNode();
	private declNode thisField;
	private fieldDeclsNode moreFields;
} 
/* End of fieldDeclsNode class */

/** Description of nullFieldDeclsNode class
 * Used for the following grammar rule
 * Fielddelcs -> λ
 * Node is created when fieldDeclsNode.NULL is called.
 */
class nullFieldDeclsNode extends fieldDeclsNode {
	nullFieldDeclsNode() {}
	boolean isNull() {return true;}

	/** Description of checkTypes method
	 * λ doesn't need type checked
	 */		
	void checkTypes() {} 
} 
/* End of nullFieldDeclsNode */

/** Description of declNode class
 * Abstract class for various types of declaration nodes
 */
// abstract superclass; only subclasses are actually created
abstract class declNode extends ASTNode {
	declNode() {super();}
	declNode(int l,int c) {super(l,c);}
} 
/* abstract class declNode */

/** Description of varDeclNode class
 * Used for the following grammar rules
 * Fielddelc -> Type id;
 * Fielddelc -> Type id = Expr;
 * Node is used to contain declaring variables and also
 * declaring and initializing at the same time.
 */
class varDeclNode extends declNode {
	varDeclNode(identNode id, typeNode t, exprNode e, int line, int col) {
		super(line,col);
		varName = id;
		varType = t;
		initValue = e;
	} 

	/** Description of checkTypes method
	 * Checks that name doesn't already exist in current scope
	 * Runs type checking on contained nodes.
	 */	
	void checkTypes() {
		//search varName.idname in symbol table
		SymbolInfo id = (SymbolInfo) st.localLookup(varName.idname);
		if (id == null) {

			//if initVale is not null
			//compare type of varType and initValue
			//compare initValue's kind
			if (initValue != exprNode.NULL) {
				initValue.checkTypes();
				typeMustBe(initValue.type.val, varType.type.val, error() + 
							"Both of the left and right " +
							"hand sides of an declared with initial value " +
							"must have the same type");
				mustBe(initValue.kind.val == Kinds.Var ||
						initValue.kind.val == Kinds.Value ||
						initValue.kind.val == Kinds.ScalarParm);
			}

			// add new symbol to scope
			id = new SymbolInfo(varName.idname,	new Kinds(Kinds.Var),varType.type);

			try {
				st.insert(id);
			}
			catch (DuplicateException d) {/* can't happen */} 
			catch (EmptySTException e) {/* can't happen */}

			varName.idinfo = id;
		} else {
			// name already exists in current scope
			System.out.println(error() + id.name() + " is already declared.");
			typeErrors++;
			varName.type = new Types(Types.Error);
		} // id != null
	}

	private final identNode varName;
	private final typeNode varType;
	private final exprNode initValue;
}
/* End of varDeclNode class */


/** Description of constDeclNode class
 * Used for the following grammar rules
 * Fielddelc -> const id = Expr;
 * Node is used to contain declaring and initializing 
 * a constant identifier
 */
class constDeclNode extends declNode {
	constDeclNode(identNode id, exprNode e,
			int line, int col) {
		super(line,col);
		constName = id;
		constValue = e;
	}

	/** Description of checkTypes method
	 * Checks that id does not exist in local scope, sets appropriate type
	 */		
	void checkTypes() {
		SymbolInfo id = (SymbolInfo) st.localLookup(constName.idname);
		if (id == null) {
			constValue.checkTypes();

			mustBe(constValue.kind.val == Kinds.Var ||
					constValue.kind.val == Kinds.Value ||
					constValue.kind.val == Kinds.ScalarParm);
			id = new SymbolInfo(constName.idname, new Kinds(Kinds.Value), constValue.type);

			try {
				st.insert(id);
			} 
			catch (DuplicateException d) {/* can't happen */}
			catch (EmptySTException e) {/* can't happen */}
			
			constName.idinfo = id;
		}
		else {
			System.out.println(error() + id.name() + " is already declared.");
			typeErrors++;
			constName.type = new Types(Types.Error);
		}

	}
	private final identNode constName;
	private final exprNode constValue;
}
/* End class constDeclNode */

/** Description of arrayDeclNode class
 * Used for the following grammar rules
 * Fielddelc -> Type id [ intlit ];
 * Node is used to contain declaring an array identifier
 */
class arrayDeclNode extends declNode {
	arrayDeclNode(identNode id, typeNode t, intLitNode lit,
			int line, int col) {
		super(line,col);
		arrayName = id;
		elementType = t;
		arraySize = lit;
	} 

	/** Description of checkTypes method
	 * Checks that id does not exist in local scope, sets appropriate type
	 * Check for appropriate array size, and typeCheck on contained nodes
	 */		
	void checkTypes() {
		SymbolInfo id;
		id = (SymbolInfo) st.localLookup(arrayName.idname);
		if (id == null) {
			id = new SymbolInfo(arrayName.idname, new Kinds(Kinds.Array), elementType.type, arraySize.getIntval());
			
			try {
				st.insert(id);
			} 
			catch (DuplicateException d) {/* can't happen */} 
			catch (EmptySTException e) {/* can't happen */}

			arrayName.idinfo = id;
		}
		else {
			System.out.println(error() + id.name() + " is already declared.");
			typeErrors++;
			arrayName.type = new Types(Types.Error);
		}

		// Check that array size is greater than 0
		if (arraySize.getIntval() < 1) {
			System.out.println(error() + "array declaration must have a positive size.");
			typeErrors++;
		}
	}
	private final identNode arrayName;
	private final typeNode elementType;
	private final intLitNode arraySize;
}
/* End of arrayDeclNode class */

/** Description of typeNode class
 * Used for the following grammar rules
 * Type -> int
 * Type -> float
 * Type -> char
 * Type -> bool
 * Abstract class node used for type nodes for declarations
 */
abstract class typeNode extends ASTNode {
// abstract superclass; only subclasses are actually created
	typeNode() {super();}
	typeNode(int l,int c, Types t) {super(l,c);type = t;}
	static nullTypeNode NULL = new nullTypeNode();
	Types type; // Used for typechecking -- the type of this typeNode
} 
/* End of abstract class typeNode */

/** Description of nullTypeNode class
 * Used in Methoddecl rules that have void return type.
 * Invoked by using typeNode.NULL
 */
class nullTypeNode extends typeNode {
	nullTypeNode() {}
	boolean isNull() {return true;}
	void checkTypes() {} 
} 
/* End of class nullTypeNode */

/** Description of intTypeNode class
 * Class node used for integer type nodes
 */
class intTypeNode extends typeNode {
	intTypeNode(int line, int col) {
		super(line,col, new Types(Types.Integer));
	} // intTypeNode
	void checkTypes() {/*No type checking needed*/} // checkTypes
} 
/* End of class intTypeNode */

/** Description of floatTypeNode class
 * Class node used for float type nodes
 */
class floatTypeNode extends typeNode {
	floatTypeNode(int line, int col) {
		super(line,col, new Types(Types.Real));
	} // intTypeNode
	void checkTypes() {/*No type checking needed*/} // checkTypes
}
/* End of class floatTypeNode */

/** Description of boolTypeNode class
 * Class node used for bool type nodes
 */
class boolTypeNode extends typeNode {
	boolTypeNode(int line, int col) {
		super(line,col, new Types(Types.Boolean));
	} // boolTypeNode
	void checkTypes() {/*No type checking needed*/} // checkTypes
}
/* End of class boolTypeNode */

/** Description of charTypeNode class
 * Class node used for char type nodes
 */
class charTypeNode extends typeNode {
	charTypeNode(int line, int col) {
		super(line,col, new Types(Types.Character));
	} // charTypeNode
	void checkTypes() {/*No type checking needed*/} // checkTypes
}
/* End of class charTypeNode */

/** Description of voidTypeNode class
 * Class node used for void type nodes
 */
class voidTypeNode extends typeNode {
	voidTypeNode(int line, int col) {
		super(line,col, new Types(Types.Void));
	} // voidTypeNode
	void checkTypes() {/*No type checking needed*/} // checkTypes
}
/* End of class voidTypeNode  */

/** Description of methodDeclsNode class
 * Used for the following grammar rules
 * Methoddecls -> Methoddecl Methoddecls
 * Methoddecls -> λ
 * Used to contain method declarations
 */
class methodDeclsNode extends ASTNode {
	methodDeclsNode() {super();}
	methodDeclsNode(methodDeclNode m, methodDeclsNode ms, int line, int col) {
		super(line,col);
		thisDecl = m;
		moreDecls = ms;
	}

	/** Description of checkTypes method
	 * Checks if more method declarations follow to determine if last method
	 * is void main(). Type checks methodDeclNode(s)
	 */		
	void checkTypes() {
		if (moreDecls != methodDeclsNode.NULL) {
			thisDecl.checkTypes();
			moreDecls.checkTypes();
		}
		else {
			if (!thisDecl.getReturnType().isNull()) {
				System.out.println(error() + "last method must be void type.");
				typeErrors++;
			}
			if (!thisDecl.getName().idname.equals("main")) {
				System.out.println(error() + "last method's name must be main.");
				typeErrors++;
			}
			thisDecl.checkTypes();
		}
	}

	static nullMethodDeclsNode NULL = new nullMethodDeclsNode();
	private methodDeclNode thisDecl;
	private methodDeclsNode moreDecls;
}
/* End of class methodDeclsNode */

/** Description of nullMethodDeclsNode class
 * Used for the following grammar rules
 * Methoddecls -> λ
 * Used to return empty method declarations. Called by 
 * methodDeclsNode.NULL.
 */
class nullMethodDeclsNode extends methodDeclsNode {
	nullMethodDeclsNode() {}
	boolean isNull() {return true;}
	void checkTypes() {} 
}
/* End of class nullMethodDeclsNode  */

/** Description of methodDeclNode class
 * Used for the following grammar rules
 * Methoddecl -> void id() {Fielddelcs Smtms} OptionalSemi
 * Methoddecl -> void id(Argdecls) {Fielddecls Stmts} OptionalSemi
 * Methoddecl -> Type id() {Fielddecls Stmts} OptionalSemi
 * Methoddecl -> Type id(Agrdecls) {Fielddecls Stmts} OptionalSemi
 * Used to contain contents of single method declarations
 */
class methodDeclNode extends ASTNode {
	methodDeclNode(identNode id, argDeclsNode a, typeNode t, fieldDeclsNode f,
			stmtsNode s, int line, int col) {
		super(line,col);
		name = id;
		args = a;
		returnType = t;
		decls = f;
		stmts = s;
	}
	typeNode getReturnType() {return returnType;}
	identNode getName() {return name;}

	/** Description of checkTypes method
	 * Checks that method name does not already exist. Handle scope and 
	 * check types of contained nodes
	 */			
	void checkTypes() {
		SymbolInfo id;
		id = (SymbolInfo) st.localLookup(name.idname);
		// Checking that method name doesn't already exist in scope
		if (id == null) {
			if (returnType.isNull())
				id = new SymbolInfo(name.idname, new Kinds(Kinds.Method), new Types(Types.Void), 
									new LinkedList<Kinds>(), new LinkedList<Types>());
			else
				id = new SymbolInfo(name.idname, new Kinds(Kinds.Method), returnType.type,
									new LinkedList<Kinds>(), new LinkedList<Types>());

			try {
				st.insert(id);
			} 
			catch (DuplicateException d) {/* can't happen */} 
			catch (EmptySTException e) {/* can't happen */}

			name.idinfo = id;
		}
		else {
			System.out.println(error() + id.name() + " is already declared.");
			typeErrors++;
			name.type = new Types(Types.Error);
		}

		currentMethod = id;

		// Open new scope for method declaration
		st.openScope();

		kindsParam = new LinkedList<Kinds>();
		typesParam = new LinkedList<Types>();

		// Check types of following arguments
		args.checkTypes();

		id.kindsParam = kindsParam;
		id.typesParam = typesParam;

		decls.checkTypes();
		stmts.checkTypes();

		// Finish up and close scope
		try {
			st.closeScope();
		}
		catch (EmptySTException e) {e.printStackTrace();}
	}
	private final identNode name;
	private final argDeclsNode args;
	private final typeNode returnType;
	private final fieldDeclsNode decls;
	private final stmtsNode stmts;
}
/* End of class methodDeclNode */

/** Description of argDeclNode class
 * Abstract super class that argument declarations
 */
// abstract superclass; only subclasses are actually created
abstract class argDeclNode extends ASTNode {
	argDeclNode() {super();}
	argDeclNode(int l,int c) {super(l,c);}
}
/* End of abstract class argDeclNode */

/** Description of argDeclsNode class
 * Used for the following grammar rules
 * ArgDecls -> Argdecl, Argdecls
 * ArgDecls -> ArgDecl
 * Node used to contain argument declarations.
 */
class argDeclsNode extends ASTNode {
	argDeclsNode() {}
	argDeclsNode(argDeclNode arg, argDeclsNode args, int line, int col) {
		super(line,col);
		thisDecl = arg;
		moreDecls = args;
	}

	/** Description of checkTypes method
	 * Type check contained nodes
	 */		
	void checkTypes() {
		thisDecl.checkTypes();
		moreDecls.checkTypes();
	}
	static nullArgDeclsNode NULL = new nullArgDeclsNode();

	private argDeclNode thisDecl;
	private argDeclsNode moreDecls;
}
/* End of class argDeclsNode */

/** Description of nullArgDeclsNode class
 * Node used to represent end of arguments. id() in Methoddecl
 * rules and Argdecls nodes that are ending. Called by
 * argDeclsNode.NULL
 */
class nullArgDeclsNode extends argDeclsNode {
	nullArgDeclsNode() {}
	boolean isNull() {return true;}
	void checkTypes() {} 
}
/* End of class nullArgDeclsNode */

/** Description of arrayArgDeclNode class
 * Used for the following grammar rule
 * Argdecl -> Type id[]
 * Contains array arguments
 */
class arrayArgDeclNode extends argDeclNode {
	arrayArgDeclNode(identNode id, typeNode t, int line, int col) {
		super(line,col);
		argName = id;
		elementType = t;
	}

	/** Description of checkTypes method
	 * Check that ident does not exist locally and has right kind.
	 * Error otherwise.
	 */		
	void checkTypes() {
		SymbolInfo id;
		id = (SymbolInfo) st.localLookup(argName.idname);
		if (id == null) {
			id = new SymbolInfo(argName.idname, new Kinds(Kinds.ArrayParm), elementType.type);

			try {
				st.insert(id);
			}
			catch (DuplicateException d) {/* can't happen */}
			catch (EmptySTException e) {/* can't happen */}

			argName.idinfo = id;

			kindsParam.add(new Kinds(Kinds.ArrayParm));
			typesParam.add(elementType.type);
		}
		else {
			System.out.println(error() + id.name() + " is already declared.");
			typeErrors++;
			argName.type = new Types(Types.Error);
		}
	}
	private final identNode argName;
	private final typeNode elementType;
}
/* End of class arrayArgDeclNode */

/** Description of valArgDeclNode class
 * Used for the following grammar rule
 * Argdecl -> Type id
 * Contains value argument declaration
 */
class valArgDeclNode extends argDeclNode {
	valArgDeclNode(identNode id, typeNode t, int line, int col) {
		super(line,col);
		argName = id;
		argType = t;
	}

	/** Description of checkTypes method
	 * Check that ident does not exist locally and has right kind.
	 * Error otherwise.
	 */		
	void checkTypes() {
		SymbolInfo id;
		id = (SymbolInfo) st.localLookup(argName.idname);
		if (id == null) {
			id = new SymbolInfo(argName.idname, new Kinds(Kinds.ScalarParm), argType.type);
			
			try {
				st.insert(id);
			}
			catch (DuplicateException d) {/* can't happen */} 
			catch (EmptySTException e) {/* can't happen */}

			argName.idinfo = id;

			kindsParam.add(new Kinds(Kinds.ScalarParm));
			typesParam.add(argType.type);
		}
		else {
			System.out.println(error() + id.name() + " is already declared.");
			typeErrors++;
			argName.type = new Types(Types.Error);
		}
	}
	private final identNode argName;
	private final typeNode argType;
}
/* End of class valArgDeclNode  */

/** Description of stmtNode class
 * Used for the following grammar rule
 * Stmt -> if (Expr) Stmts endif
 * Stmt -> if (Expr) Stmts else Stmts endif
 * Stmt -> while (Expr) Stmts
 * Stmt -> id : while(Expr) Stmts
 * Stmt -> Name = Expr;
 * Stmt -> read(Readlist);
 * Stmt -> print(Printlist);
 * Stmt -> id();
 * Stmt -> id(Args);
 * Stmt -> return;
 * Stmt -> return Expr;
 * Stmt -> break id;
 * Stmt -> continue id;
 * Stmt -> ++id; | id++; | --id; | id--;
 * Stmt -> for (ident = expr; expr; update) Stmts
 * Stmt -> {Fielddecls Stmts} OptionalSemi
 * abstract superclass used to contain stmt rules.
 * All stmt subclasses are used to create specific types of stmt nodes
 */
// abstract superclass; only subclasses are actually created
abstract class stmtNode extends ASTNode {
	stmtNode() {super();}
	stmtNode(int l,int c) {super(l,c);}
	static nullStmtNode NULL = new nullStmtNode();
}
/* End of abstract class stmtNode */

/** Description of nullStmtNode class
 * Used to return empty stmtNode. Invoked by stmtNode.NULL
 */
class nullStmtNode extends stmtNode {
	nullStmtNode() {}
	boolean isNull() {return true;}
	void checkTypes() {} 
} 
/* End of class nullStmtNode */

/** Description of stmtsNode class
 * Used for the following grammar rules
 * Stmts ->  Stmt Stmts
 * Stmts -> Stmt
 * Node contains Stmt node and Stmts node that follows
 */
class stmtsNode extends ASTNode {
	stmtsNode(stmtNode stmt, stmtsNode stmts, int line, int col) {
		super(line,col);
		thisStmt = stmt;
		moreStmts = stmts;
	}
	stmtsNode() {}

	/** Description of checkTypes method
	 *  Type check contained nodes
	 */		
	void checkTypes() {
		thisStmt.checkTypes();
		moreStmts.checkTypes();
	} 

	static nullStmtsNode NULL = new nullStmtsNode();
	private stmtNode thisStmt;
	private stmtsNode moreStmts;
}
/* End of class stmtsNode */

/** Description of nullStmtsNode class
 * Used for the following grammar rule
 * Stmts -> Stmt
 * Node used to represent empty/lack of StmtsNode.
 * Invoked by stmtsNode.NULL
 */
class nullStmtsNode extends stmtsNode {
	nullStmtsNode() {}
	boolean isNull() {return true;}
	void checkTypes() {} 
}
/* End of class nullStmtsNode */

/** Description of asgNode class
 * Used for the following grammar rule
 * Stmts ->  Name = Expr;
 * Node contains single assignment statment
 */
class asgNode extends stmtNode {
	asgNode(nameNode n, exprNode e, int line, int col) {
		super(line,col);
		target = n;
		source = e;
	}

	/** Description of checkTypes method
	 *  Type check contained nodes
	 */		
	void checkTypes() {
		target.checkTypes();

		// Check that target of assignment is correct kind
		mustBe(target.kind.val == Kinds.Var ||
				target.kind.val == Kinds.Array ||
				target.kind.val == Kinds.ScalarParm);

		source.checkTypes();

		// Check that target and source have matching type if target is
		// variable or scalar
		if (target.kind.val == Kinds.Var || target.kind.val == Kinds.ScalarParm) {

			mustBe(source.kind.val == Kinds.Var || source.kind.val == Kinds.Value ||
					source.kind.val == Kinds.ScalarParm || source.kind.val == Kinds.Method);
			typeMustBe(target.type.val, source.type.val, error() +
					"target and source must have same type");
		}
		// Otherwise, ensure that source kind is an value or array
		else {
			mustBe(source.kind.val == Kinds.Value || source.kind.val == Kinds.Array);
			
			if (source.kind.val == Kinds.Value) {
				if (source.type.val == Types.String) {
					if (((SymbolInfo) st.localLookup(target.getVarName().idname)).arraySize !=
							((strLitNode) source).length) {
						System.out.println(error() + "array size of target and source must be same");
						typeErrors++;
					}
				}
				else {
					typeMustBe(target.type.val, Types.Character, error() + 
								"target must be character");
					typeMustBe(source.type.val, Types.String, error() +
								"source must be string");
				}
			}
			else if (source.kind.val == Kinds.Array) {
				typeMustBe(target.type.val, source.type.val, error() +
							"target and source must be same type");
				if (((SymbolInfo) st.localLookup(target.getVarName().idname)).arraySize !=
						((SymbolInfo) st.localLookup(((nameNode)source).getVarName().idname)).arraySize) {
					System.out.println(error() + "array size of target and source must be same");
					typeErrors++;
				}
			}
		}
	}

	private final nameNode target;
	private final exprNode source;
}
/* End of class asgNode */

/** Description of ifThenNode class
 * Used for the following grammar rule
 * Stmt -> if (Expr) Stmts
 * Stmt -> if (Expr) Stmts else Stmts
 * Node contains if then/if then else statements and contained statements
 */
class ifThenNode extends stmtNode {
	ifThenNode(exprNode e, stmtsNode s1, stmtsNode s2, int line, int col) {
		super(line,col);
		condition = e;
		thenPart = s1;
		elsePart = s2;
	}

	/** Description of checkTypes method
	 *  Type check contained nodes, check that condition is boolean expression
	 */		
	void checkTypes() {
		condition.checkTypes();
		typeMustBe(condition.type.val, Types.Boolean, error() + 
					"The control expression of an" + " if must be a bool.");
		
		thenPart.checkTypes();
		elsePart.checkTypes();

	} 

	private final exprNode condition;
	private final stmtsNode thenPart;
	private final stmtsNode elsePart;
}
/* End of class ifThenNode */

/** Description of ifThenNode class
 * Used for the following grammar rule
 * Stmt -> while (Expr) Stmts
 * Stmt -> id : while (Expr) Stmts
 * Node contains while statements with label support
 */
class whileNode extends stmtNode {
	whileNode(exprNode i, exprNode e, stmtNode s, int line, int col) {
		super(line,col);
		label = i;
		condition = e;
		loopBody = s;
	}

	/** Description of checkTypes method
	 *  Type check contained nodes.
	 */	
	void checkTypes() {
		// Check kind of condition, and check results in bool result (type)
		condition.checkTypes();
		mustBe(condition.kind.val == Kinds.Var ||
				condition.kind.val == Kinds.Value ||
				condition.kind.val == Kinds.ScalarParm);
		typeMustBe(condition.type.val, Types.Boolean, error() + 
					"The control expression of an" + " if must be a bool.");

		// Check type of while contents if no label
		if (label.isNull()) {
			loopBody.checkTypes();
		}
		// Else label logic
		else {
			SymbolInfo id;
			id = (SymbolInfo) st.localLookup(((identNode)label).idname);
			// If label doesn't exist in scope
			if (id == null) {
				id = new SymbolInfo(((identNode)label).idname, new Kinds(Kinds.Label), new Types(Types.Void));
				
				try {
					st.insert(id);
				} 
				catch (DuplicateException d) {/* can't happen */}
				catch (EmptySTException e) {/* can't happen */}

				((identNode)label).idinfo = id;
			}
			else {
				mustBe(id.kind.val == Kinds.HiddenLabel);
				id.kind.val = Kinds.Label;
			}
			loopBody.checkTypes();
			// Switch to hidden so can be used when scope is checked.
			id.kind.val = Kinds.HiddenLabel;
		}
	}
	private final exprNode label;
	private final exprNode condition;
	private final stmtNode loopBody;
}
/* End of class whileNode */

/** Description of readNode class
 * Used for the following grammar rule
 * Readlist -> Name, Readlist
 * Readlist -> Name
 * Node contains readlist statement
 */
class readNode extends stmtNode {
	readNode() {}
	readNode(nameNode n, readNode rn, int line, int col) {
		super(line,col);
		targetVar = n;
		moreReads = rn;
	}

	/** Description of checkTypes method
	 * Type check contained nodes, and make sure that only approved
	 * types are read
	 */		
	void checkTypes() {
		targetVar.checkTypes();
		
		mustBe(targetVar.kind.val == Kinds.Var ||
				targetVar.kind.val == Kinds.Scalar ||
				targetVar.kind.val == Kinds.ScalarParm);
		
		if (targetVar.type.val != Types.Integer &&
			targetVar.type.val != Types.Character)
				typeMustBe(targetVar.type.val, Types.Integer,
							error() + "Only int or char values may be readed.");

		moreReads.checkTypes();
	}
	static nullReadNode NULL = new nullReadNode();
	private nameNode targetVar;
	private readNode moreReads;
}
/* End of class readNode */

/** Description of nullReadNode class
 * Node used for ending series of names in readlist.
 * Invoked by readNode.NULL
 */
class nullReadNode extends readNode {
	nullReadNode() {}
	boolean isNull() {return true;}
	void checkTypes() {} 
}
/* End of class nullReadNode */

/** Description of printNode class
 * Used for the following grammar rule
 * Printlist -> Expr, Readlist
 * Printlist -> Expr
 * Node contains readlist statement
 */
class printNode extends stmtNode {
	printNode() {}
	printNode(exprNode val, printNode pn, int line, int col) {
		super(line,col);
		outputValue = val;
		morePrints = pn;
	}
	static nullPrintNode NULL = new nullPrintNode();

	/** Description of checkTypes method
	 * Type check contained nodes, and make sure only approved types/kinds
	 * are printed.
	 */		
	void checkTypes() {
		outputValue.checkTypes();
		if (outputValue.type.val != Types.Integer &&
			outputValue.type.val != Types.Character &&
			outputValue.type.val != Types.Boolean &&
			outputValue.type.val != Types.Real &&
			outputValue.type.val != Types.String &&
			(outputValue.type.val != Types.Character &&
			 outputValue.kind.val != Kinds.Array))
				typeMustBe(outputValue.type.val, Types.Integer,
							error() + "Only int, char, bool, and float values, char arrays, and string " +
							"literals may be printed.");
		morePrints.checkTypes();
	}

	private exprNode outputValue;
	private printNode morePrints;
} 
/* End of class printNode */

/** Description of nullPrintNode class
 * Node used for ending series of expressions in printlist
 * Invoked by printNode.NULL
 */
class nullPrintNode extends printNode {
	nullPrintNode() {}
	boolean isNull() {return true;}
	void checkTypes() {} 
}
/* End of class nullPrintNode */

/** Description of callNode class
 * Used for the following grammar rule
 * Stmt -> id();
 * Stmt -> id(Args);
 * Node contains function calling statement
 */
class callNode extends stmtNode {
	callNode(identNode id, argsNode a, int line, int col) {
		super(line,col);
		methodName = id;
		args = a;
	}

	/** Description of checkTypes method
	 * Check that id exists in symbol table, and ensure
	 * all conditions are met for proper function call.
	 * Type check argsNode if not set to null
	 */		
	void checkTypes() {

		SymbolInfo id;
		// Error if id not declared
		id = (SymbolInfo) st.globalLookup(methodName.idname);
		if (id == null) {
			System.out.println(error() + methodName.idname + " method is not declared.");
			typeErrors++;
			methodName.type = new Types(Types.Error);
		}
		else {
			// check type and kind
			typeMustBe(id.type.val, Types.Void, error() + methodName.idname + " must be void type");
			mustBe(id.kind.val == Kinds.Method);
			
			kindsParam = new LinkedList<Kinds>();
			typesParam = new LinkedList<Types>();
			
			// Check argsNode if not null
			if (!args.isNull())
				args.checkTypes();

			LinkedList<Kinds> methodKindsParam  = id.kindsParam;
			LinkedList<Types> methodTypesParam  = id.typesParam;

			//size don't match => don't check args
			if (kindsParam.size() == methodKindsParam.size()) {
				int size = kindsParam.size();
				for (int i = 0; i < size; i++) {
					typeMustBe(typesParam.get(i).val, methodTypesParam.get(i).val, error() +
							"type of parameter must match to the method's type of parameter");
					
					if (kindsParam.get(i).val == Kinds.Var || 
							kindsParam.get(i).val == Kinds.Value ||
							kindsParam.get(i).val == Kinds.ScalarParm)
						mustBe(methodKindsParam.get(i).val == Kinds.ScalarParm);
					else if (kindsParam.get(i).val == Kinds.Array ||
								kindsParam.get(i).val == Kinds.ArrayParm)
						mustBe(methodKindsParam.get(i).val == Kinds.ArrayParm);
				}
			}
			else {
				System.out.println(error() + "size of args don't match to the size of the method");
				typeErrors++;
			}
		}
	}

	private final identNode methodName;
	private final argsNode args;
}
/* End of class callNode */

/** Description of returnNode class
 * Used for the following grammar rule
 * Stmt -> return;
 * Stmt -> return Expr;
 * Node contains return statement with or without returned expression
 */
class returnNode extends stmtNode {
	returnNode(exprNode e, int line, int col) {
		super(line,col);
		returnVal = e;
	}

	/** Description of checkTypes method
	 * Type check exprNode
	 */			
	void checkTypes() {
		returnVal.checkTypes();
		// If expr is null, type should be void
		if (returnVal.isNull())
			typeMustBe(Types.Void, currentMethod.type.val, error() +
				"empty return statement expected for this method.");
		// Other wise, make sure type of expr matched expected return type
		// of the current method
		else
			typeMustBe(returnVal.type.val, currentMethod.type.val, error() +
				"return type of returned expression must match return type of method.");
	}
	private final exprNode returnVal;
}
/* End of class returnNode */

/** Description of blockNode class
 * Used for the following grammar rule
 * Stmt -> {Fielddecls Stmts} OptionalSemi
 * Node used to contain block statements
 */
class blockNode extends stmtNode {
	blockNode(fieldDeclsNode f, stmtsNode s, int line, int col) {
		super(line,col);
		decls = f;
		stmts = s;
	}

	/** Description of checkTypes method
	 * Opens new scope for code block, check type of contained nodes,
	 * and close the scope.
	 */			
	void checkTypes() {
		st.openScope();
		decls.checkTypes();
		stmts.checkTypes();
		try {
			st.closeScope();
		}
		catch (EmptySTException e) {e.printStackTrace();}
	}
	private final fieldDeclsNode decls;
	private final stmtsNode stmts;
}
/* End of class blockNode */

/** Description of incNode class
 * Used for the following grammar rule
 * Stmt -> ++id; | id++;
 * Node used to contain increment statements
 */
class incNode extends stmtNode {
	incNode(identNode i, int incPos, int line, int col) {
		super(line, col);
		this.ident = i;
		this.incPos = incPos;
	}

	/** Description of checkTypes method
	 * Type check id, ensure expected kind and type.
	 */				
	void checkTypes() {
		ident.checkTypes();
		mustBe(ident.kind.val == Kinds.Var || ident.kind.val == Kinds.ScalarParm);
		if (ident.kind.val != Types.Integer && ident.kind.val != Types.Real &&
			ident.kind.val != Types.Character) {
			System.out.println(error() + "ident must be int, float, or char type");
			typeErrors++;
		}
	}

	private identNode ident;
	private int incPos;
}
// End of incNode class.

/** Description of decNode class
 * Used for the following grammar rule
 * Stmt -> --id; | id--;
 * Node used to contain decrement statements
 */
class decNode extends stmtNode {
	decNode(identNode i, int decPos, int line, int col) {
		super(line, col);
		this.ident = i;
		this.decPos = decPos;
	}

	/** Description of checkTypes method
	 * Type check id, ensure expected kind and type.
	 */				
	void checkTypes() {
		ident.checkTypes();
		mustBe(ident.kind.val == Kinds.Var || ident.kind.val == Kinds.ScalarParm);
		if (ident.kind.val != Types.Integer && ident.kind.val != Types.Real &&
			ident.kind.val != Types.Character) {
			System.out.println(error() + "ident must be int, float, or char type");
			typeErrors++;
		}
	}

	private identNode ident;
	private int decPos;
}
/* End of decNode class */

/** Description of forNode class
 * Used for the following grammar rule
 * Stmt -> for (ident = expr; expr; update) Stmts
 * Node used to contain for statement and contents
 */
class forNode extends stmtNode {
	forNode(identNode initId, exprNode initE, exprNode condE, 
			updateNode update, stmtNode s, int line, int col) {
		super(line, col);
		this.initId = initId;
		this.initE = initE;
		this.condE = condE;
		this.update = update;
		this.stmt = s;
	}

	/** Description of checkTypes method
	 * Type check id node and expression. Ensure proper kind of id is
	 * being used in initializing statement. Report back appropriate errors
	 * for issues with type or kind.
	 */		
	void checkTypes() {
		initId.checkTypes();		

		mustBe(initId.kind.val == Kinds.Var ||
				initId.kind.val == Kinds.Value ||
				initId.kind.val == Kinds.Array ||
				initId.kind.val == Kinds.ScalarParm);

		initE.checkTypes();

		typeMustBe(initId.type.val, initE.type.val, error() + 
					"initial var and initial value must have same type");

		if (initId.kind.val == Kinds.Var || initId.kind.val == Kinds.ScalarParm) {
			mustBe(initE.kind.val == Kinds.Var || initE.kind.val == Kinds.ScalarParm);
			typeMustBe(initId.type.val, initE.type.val, error() +
						"initId and initE must have same type");
		}
		else if (initId.kind.val == Kinds.Array || initId.kind.val == Kinds.ArrayParm) {
			mustBe(initE.kind.val == Kinds.Array || initE.kind.val == Kinds.ArrayParm);
			if (initId.type.val != initE.type.val)
				if (initId.type.val != Types.Character || initE.type.val != Types.String) {
					System.out.println(error() + "initId and initE must be same type or " +
												"initId must be Character and " +
												"initE must be String");
					typeErrors++;
				}
		}

		condE.checkTypes();
		typeMustBe(condE.type.val, Types.Boolean, 
					error() + "boolean required for control expression");

		update.checkTypes();
		stmt.checkTypes();
	}
	
	private identNode initId;
	private exprNode initE;
	private exprNode condE;
	private updateNode update;
	private stmtNode stmt;
} 
/* End of class forNode

/** Description of update class
 * Node used to contain update portion of for node
 */
class updateNode extends stmtNode {
	updateNode(int updateCond, identNode updateId, exprNode updateE, 
			   int symPos, int line, int col) {
		super(line, col);
		this.updateCond = updateCond;
		this.updateId = updateId;
		this.updateE = updateE;
		this.symPos = symPos;
	}

	/** Description of checkTypes method
	 * Type check id node and expression. Ensure that 
	 * update var matches type of value, or that update expression exists
	 */			
	void checkTypes() {
		updateId.checkTypes();

		if (!updateE.isNull()) {
			updateE.checkTypes();
			//need check kind
			typeMustBe(updateId.type.val, updateE.type.val, error() +
						"update var and update value must have same type");

			if (updateId.kind.val == Kinds.Var || updateId.kind.val == Kinds.ScalarParm) {
			mustBe(updateE.kind.val == Kinds.Var || updateE.kind.val == Kinds.ScalarParm);
			typeMustBe(updateId.type.val, updateE.type.val, error() +
						"updateId and updateE must have same type");
			}
			else if (updateId.kind.val == Kinds.Array || updateId.kind.val == Kinds.ArrayParm) {
				mustBe(updateE.kind.val == Kinds.Array || updateE.kind.val == Kinds.ArrayParm);
				if (updateId.type.val != updateE.type.val)
					if (updateId.type.val != Types.Character || updateE.type.val != Types.String) {
						System.out.println(error() + "updateId and updateE must be same type or " +
												"updateId must be Character and " +
												"updateE must be String");
						typeErrors++;
					}
			}
		}
		else {
			mustBe(updateId.kind.val == Kinds.Var || updateId.kind.val == Kinds.ScalarParm);
			if (updateId.kind.val != Types.Integer && updateId.kind.val != Types.Real &&
					updateId.kind.val != Types.Character) {
				System.out.println(error() + "ident must be int, float, or char type");
				typeErrors++;
			}
		}
	}

	private int updateCond;
	private identNode updateId;
	private exprNode updateE;
	private int symPos;
	private int line;
	private int col;
}
/* End of updateNode class */

/** Description of breakNode class
 * Used for the following grammar rule
 * Stmt -> break id;
 * Node that contains break statement
 */
class breakNode extends stmtNode {
	breakNode(identNode i, int line, int col) {
		super(line,col);
		label = i;
	}

	/** Description of checkTypes method
	 * Check that id exists and is of type label, meaning it belongs to
	 * an outside while statement
	 */				
	void checkTypes() {
		SymbolInfo id;
		id = (SymbolInfo) st.globalLookup(label.idname);
		if (id == null) {
			System.out.println(error() + "label " + label.idname + 
								" not found in global scope.");
			typeErrors++;
		}
		else
			mustBe(id.kind.val == Kinds.Label);
	}
	private final identNode label;
} // class breakNode 

/** Description of continueNode class
 * Used for the following grammar rule
 * Stmt -> continue id;
 * Node that contains break statement
 */
class continueNode extends stmtNode {
	continueNode(identNode i, int line, int col) {
		super(line,col);
		label = i;
	}

	/** Description of checkTypes method
	 * Check that id exists and is of type label, meaning it belongs to
	 * an outside while statement
	 */			
	void checkTypes() {
		SymbolInfo id;
		id = (SymbolInfo) st.globalLookup(label.idname);
		if (id == null) {
			System.out.println(error() + "label " + label.idname + 
								" not found in global scope.");
			typeErrors++;
		}
		else
			mustBe(id.kind.val == Kinds.Label);
	}
	private final identNode label;
}
/* End of class continueNode */

/** Description of argsNode class
 * Used for the following grammar rule
 * Args -> Expr, Args
 * Args -> Expr
 * Node for containing passed agruments
 */
class argsNode extends ASTNode {
	argsNode() {}
	argsNode(exprNode e, argsNode a, int line, int col) {
		super(line,col);
		argVal = e;
		moreArgs = a;
	}

	/** Description of checkTypes method
	 *  Type check argument node, copy kinds and type. Check type of
	 * follow arguments node if more exist,
	 */		
	void checkTypes() {
		argVal.checkTypes();
		
		kindsParam.add(argVal.kind);
		typesParam.add(argVal.type);
		
		if (!moreArgs.isNull())
			moreArgs.checkTypes();
	}
	static nullArgsNode NULL = new nullArgsNode();
	private exprNode argVal;
	private argsNode moreArgs;
} // class argsNode 

/** Description of nullArgsNode class
 * Used to represent empty arguments statement. Invoked
 * by argsNode.NULL
 */
class nullArgsNode extends argsNode {
	nullArgsNode() {}
	boolean isNull() {return true;}
}
/* end of class nullArgsNode */

/** Description of exprNode class
 * Used for the following grammar rules
 * Expr ->  Expr || Term
 * Expr -> Expr && Term
 * Expr -> Term
 * Abstract superclass used to contain expressions
 */
// abstract superclass; only subclasses are actually created
abstract class exprNode extends ASTNode {
	exprNode() {super();}
	exprNode(int l,int c) {
		super(l,c);
		type = new Types();
		kind = new Kinds();
	} 
	exprNode(int l,int c,Types t,Kinds k) {
		super(l,c);
		type = t;
		kind = k;
	}
	static nullExprNode NULL = new nullExprNode();
		protected Types type; // Used for typechecking: the type of this node
		protected Kinds kind; // Used for typechecking: the kind of this node
} 
/* end of abstract class exprNode */

/** Description of nullExprNode class
 * Used to represent empty expression statement. Invoked
 * by exprNode.NULL
 */
class nullExprNode extends exprNode {
	nullExprNode() {super();}
	boolean isNull() {return true;}
	void checkTypes() {} 
}
/* end of class nullExprNode */

/** Description of binaryOpNode class
 * Used for the following grammar rules
 * Term -> Factor < Factor
 * Term -> Factor > Factor
 * Term -> Factor <= Factor
 * Term -> Factor >= Factor
 * Term -> Factor == Factor
 * Term -> Factor != Factor
 * Node used to contain expressions using binary operators
 */
class binaryOpNode extends exprNode {
	binaryOpNode(exprNode e1, int op, exprNode e2, int line, int col) {
		super(line, col, new Types(Types.Unknown), 
							new Kinds(Kinds.Value));
		operatorCode = op;
		leftOperand = e1;
		rightOperand = e2;
	}

	/** Description of toString method
	 *  Return string equivalent of operator
	 * @param op 	Sym value of operator
	 */		
	static String toString(int op) {
		String out = " ";
		switch (op) {
			case sym.COR:
				out = " || ";
				break;
			case sym.CAND:
				out = " && ";
				break;
			case sym.LT:
				out = " < ";
				break;
			case sym.GT:
				out = " > ";
				break;
			case sym.LEQ:
				out = " <= ";
				break;
			case sym.GEQ:
				out = " >= ";
				break;
			case sym.EQ:
				out = " == ";
				break;
			case sym.NOTEQ:
				out = " != ";
				break;
			case sym.PLUS:
				out = " + ";
				break;
			case sym.MINUS:
				out = " - ";
				break;
			case sym.TIMES:
				out = " * ";
				break;
			case sym.SLASH:
				out = " / ";
				break;
			default:
				mustBe(false);
				break;
		} // switch(op)
		return out;
	} // toString

	/** Description of checkTypes method
	 *  Type check left and right operands, ensure legal operation,
	 *  and that different types of operators follow expected function
	 */			
	void checkTypes() {
		// Check that given operator is valid binary operation
		mustBe(	operatorCode==sym.COR	|| operatorCode==sym.CAND	||
				operatorCode==sym.LT	|| operatorCode==sym.GT		||
				operatorCode==sym.LEQ	|| operatorCode==sym.GEQ	||
				operatorCode==sym.EQ	|| operatorCode==sym.NOTEQ	||
				operatorCode==sym.PLUS	|| operatorCode==sym.MINUS	||
				operatorCode==sym.TIMES	|| operatorCode==sym.SLASH	);

		leftOperand.checkTypes();
		rightOperand.checkTypes();

		// Checks for logical operators, sets type of the binaryOpNode to bool
		if (operatorCode == sym.COR || operatorCode == sym.CAND) {
			
			// Regardless of operand type, this node is expected to be
			// of type boolean. This reduces repeated errors.
			type = new Types(Types.Boolean);

			// Checks that both operands are booleans
			typeMustBe(leftOperand.type.val, Types.Boolean,
				error() + "Left operand of" + toString(operatorCode) +
						"must be a bool.");
			typeMustBe(rightOperand.type.val, Types.Boolean, 
				error() + "Right operand of" + toString(operatorCode) + 
						"must be a bool.");
		}

		// Checks for arithmetic operators, sets type of binaryOpNode to 
		// int or real/float as appropriate
		else if (operatorCode == sym.PLUS || operatorCode == sym.MINUS ||
					operatorCode == sym.TIMES || operatorCode == sym.SLASH	) {
			
			// Checks for integer arithmetic, sets type of binaryOpNode
			// to int if appropriate
			if (leftOperand.type.val == Types.Integer || 
				leftOperand.type.val == Types.Character) {
				
				type = new Types(Types.Integer);
				
				// If right op doesn't match left
				if (rightOperand.type.val != Types.Integer &&
					rightOperand.type.val != Types.Character)
					typeMustBe(rightOperand.type.val, Types.Integer,
						error() + "Right operand of" + toString(operatorCode) + 
						"must be a int or char.");
			}
			// Checks for float arithmetic, sets type of binaryOpNode to
			// real/float if appropriate
			else if (leftOperand.type.val == Types.Real) {
				type = new Types(Types.Real);
				
				// if right op doesn't match left
				if (rightOperand.type.val != Types.Real) {
/*
					typeMustBe(rightOperand.type.val, Types.Real,
						error() + "Float expected for float arithmetic.");
*/
					typeMustBe(leftOperand.type.val, Types.Integer,
							error() + "left operand expected for int or char");
					typeMustBe(rightOperand.type.val, Types.Integer,
							error() + "right operand expected for int or char");
					
					type = new Types(Types.Integer);
				}
				// else, right op matches
				else
					type = new Types(Types.Real);
			}
			// else, left op is not int, char, or float/real, and is
			// an invalid type for arithmetic operators.
			// We default type to int to prevent repeat errors
			else {
				typeMustBe(leftOperand.type.val, Types.Integer,
						error() + "Left operand of" + toString(operatorCode) + 
						"must be a int or char.");
				typeMustBe(rightOperand.type.val, Types.Integer,
						error() + "Right operand of" + toString(operatorCode) + 
						"must be a int or char.");
				//System.out.println(error() + "Incorrect arithmetic operands.");
				typeErrors++;
				type = new Types(Types.Integer);
			}
		}

		// Logic for remaining operators, which are all relational.
		// block above will catch any invalid operators.
		else {
			// Boolean type is expected regardless of errors
			type = new Types(Types.Boolean);

			// If left op is arth value
			if (leftOperand.type.val == Types.Integer ||
				leftOperand.type.val == Types.Character ||
				leftOperand.type.val == Types.Real) {

				// and right op doesn't match arth value
				if (rightOperand.type.val != Types.Integer &&
					rightOperand.type.val != Types.Character &&
					rightOperand.type.val != Types.Real) {
					System.out.println(error() + "Arithmetic value expected.");
					typeErrors++;
				}
			}

			// Else if left op is boolean
			else if (leftOperand.type.val == Types.Boolean)
					// and right op doesn't match boolean
					typeMustBe(rightOperand.type.val, Types.Boolean,
						error() + "Right operand of" + toString(operatorCode) +
						"must be a boolean");

			// Else left op isn't value type
			else {
				// If right is boolean, left expected to match
				if (rightOperand.type.val == Types.Boolean)
					typeMustBe(leftOperand.type.val, Types.Boolean,
						error() + "Left operand of" + toString(operatorCode) +
						"expected to be boolean");
				// Else if right is arth value, left expected to match
				else if (rightOperand.type.val == Types.Integer ||
						rightOperand.type.val == Types.Real ||
						rightOperand.type.val == Types.Character)
							typeMustBe(leftOperand.type.val, Types.Integer,
								error() + "Left operand of" + toString(operatorCode) +
								"expected to be integer, float, or character");	
				// Otherwise, neither left/right ops are correct
				else 
					typeMustBe(leftOperand.type.val, Types.Boolean,
						error() + "Left operand of" + toString(operatorCode) +
						"must be of integer, float, character, or boolean type");
			}
		}
	}
	private final exprNode leftOperand;
	private final exprNode rightOperand;
	private final int operatorCode; // Token code of the operator
}
/* end of class binaryOpNode  */

/** Description of unaryOpNode class
 * Used for the following grammar rule
 * Unary -> !Unary
 * Node used to contain expressions using unary operators
 */
class unaryOpNode extends exprNode {
	unaryOpNode(int op, exprNode e, int line, int col) {
		super(line,col);
		operand = e;
		operatorCode = op;
	} // unaryOpNode

	/** Description of printOp method
	 * Prints out appropriate operator, "!" in this case or error
 	 * @param op 	integer value of operator
 	 */		
	static String toString(int op) {
		switch (op) {
			case sym.NOT:
				System.out.print(" ! ");
				break;
			default:
				mustBe(false);
		}
		return "";
	}

	/** Description of checkTypes method
	 *  Must have ! operator, check type of operandNode and ensure
	 * it contains boolean expression
	 */		
	void checkTypes() {
		mustBe(operatorCode == sym.NOT);

		operand.checkTypes();

		if (operand.type.val == Types.Boolean) 
			type = new Types(Types.Boolean);
		else {
			System.out.println(error() + "operand must be a bool");
			type = new Types(Types.Error);
		}
	}
	private exprNode operand;
	private int operatorCode; // Token code of the operator
} 
/* end of class unaryOpNode */

/** Description of castNode class
 * Used for the following grammar rule
 * Unary -> (Type) Unary
 * Node used contain casting on an expression
 */
class castNode extends exprNode {
	castNode(typeNode t, exprNode e, int line, int col) {
		super(line,col);
		operand = e;
		resultType = t;
	} 


	/** Description of checkTypes method
	 *  Type check exprNode, and make sure attempted casting is legal
	 */		
	void checkTypes() {
		operand.checkTypes();

		type = resultType.type;

		if (resultType.type.val != Types.Integer &&	resultType.type.val != Types.Character &&
			resultType.type.val != Types.Real && resultType.type.val != Types.Boolean) {
			System.out.println(error() + "type of type-cast is illegal");
			typeErrors++;
			type = new Types(Types.Error);
		}

		if (operand.type.val != Types.Integer && operand.type.val != Types.Character &&
				operand.type.val != Types.Boolean) {
			System.out.println(error() + "the type of expression require int, char, or bool");
			typeErrors++;
			type = new Types(Types.Error);
		}
	}

	private final exprNode operand;
	private final typeNode resultType;
}
/* End of class castNode */

/** Description of fctCallNode class
 * Used for the following grammar rules
 * Unit -> id()
 * Unit -> id(Args)
 * Node used to contain expression involving a function call
 */
class fctCallNode extends exprNode {
	fctCallNode(identNode id, argsNode a, int line, int col) {
		super(line,col);
		methodName = id;
		methodArgs = a;
	}

	/** Description of checkTypes method
	 *  Type check identNode and argsNode if not null
	 */			
	void checkTypes() {
		methodName.checkTypes();
		
		kind = new Kinds(Kinds.Method);
		type = methodName.type;

		kindsParam = new LinkedList<Kinds>();
		typesParam = new LinkedList<Types>();
		
		if (!methodArgs.isNull())
			methodArgs.checkTypes();

		LinkedList<Kinds> methodKindsParam = currentMethod.kindsParam;
		LinkedList<Types> methodTypesParam = currentMethod.typesParam;
		
		//size don't match => don't check args
		if (kindsParam.size() == methodKindsParam.size()) {
			int size = kindsParam.size();
			for (int i = 0; i < size; i++) {		
				typeMustBe(typesParam.get(i).val, methodTypesParam.get(i).val, error() +
							"type of parameter must match to the method's type of parameter");
				mustBe(kindsParam.get(i).val == methodKindsParam.get(i).val);
			}
		}
		else {
			System.out.println(error() + "size of args don't match to the size of the method");
			type = new Types(Types.Error);
			typeErrors++;
		}
	}
	private final identNode methodName;
	private final argsNode methodArgs;
} 
/* end of class fctCallNode */

/** Description of identNode class
 * Node used to contain identifer literal
 */
class identNode extends exprNode {
	identNode(String identname, int line, int col) {
		super(line,col,new Types(Types.Unknown), new Kinds(Kinds.Var));
		idname = identname;
		nullFlag = false;
	} // identNode

	identNode(boolean flag) {
		super(0,0,new Types(Types.Unknown), new Kinds(Kinds.Var));
		idname = "";
		nullFlag = flag;
	} // identNode

	boolean isNull() {return nullFlag;} // Is this node null?

	static identNode NULL = new identNode(true);

	/** Description of checkTypes method
	 * Check all scopes for existing idname
	 */		
	void checkTypes() {
		SymbolInfo id;
		id = (SymbolInfo) st.globalLookup(idname);

		if (id == null) {
			System.out.println(error() + idname + " is not declared.");
			typeErrors++;
			type = new Types(Types.Error);
		} else {
			type = id.type; 
			kind = id.kind;
			idinfo = id; // Save ptr to correct symbol table entry
		} // id != null
	} // checkTypes

	public String idname;
	public SymbolInfo idinfo; // symbol table entry for this ident
	private final boolean nullFlag;
} 
/* End of class identNode */

/** Description of nameNode class
 * Used for the following grammar rules
 * Name -> id
 * Name -> id [Expr]
 * Node contains name content, id or array id with expression
 */
class nameNode extends exprNode {
	nameNode(identNode id, exprNode expr, int line, int col) {
		super(line,col,new Types(Types.Unknown), new Kinds(Kinds.Var));
		varName = id;
		subscriptVal = expr;
	} // nameNode
	identNode getVarName() {return varName;}

	/** Description of checkTypes method
	 * Check for array indexing, type check node
	 */			
	void checkTypes() {
		varName.checkTypes();

		// check if an array is not being indexed
		if (subscriptVal == exprNode.NULL) {
			type = varName.type;
			kind = varName.kind;
		}
		// Else, an array is being indexed
		else {
			// Check that appropriate kind is used
			mustBe(subscriptVal.kind.val == Kinds.Value ||
					subscriptVal.kind.val == Kinds.Var ||
					subscriptVal.kind.val == Kinds.ScalarParm);
			// The exprNode is checked to be of int or char
			// type for array to be indexed
			if (subscriptVal.type.val != Types.Integer &&
				subscriptVal.type.val != Types.Character) {
				System.out.println(error() + "array must be indexed by int or character expression");
				typeErrors++;
			}
			mustBe(varName.kind.val == Kinds.Array || varName.kind.val == Kinds.ArrayParm);
			type = varName.type;
			kind = new Kinds(Kinds.Var);
		}
	} 

	private final identNode varName;
	private final exprNode subscriptVal;
}
/* End of class nameNode */

class intLitNode extends exprNode {
	intLitNode(int val, int line, int col) {
		super(line,col,new Types(Types.Integer), new Kinds(Kinds.Value));
		intval = val;
	} 
	int getIntval() { return intval;}
	
	/** Description of checkTypes method
	 * Since intLitNode will always contain a integer value,
	 * set in constructor
	 */			 
	void checkTypes() {	} 

	private final int intval;
}
/* End class intLitNode */

/** Description of floatLitNode class
 * Node for float literal
 */
class floatLitNode extends exprNode {
	floatLitNode(float val, int line, int col) {
		super(line,col,new Types(Types.Real), new Kinds(Kinds.Value));
		floatval = val;
	}

	/** Description of checkTypes method
	 * Since floatLitNode will always contain a float value,
	 * set in constructor
	 */			 
	void checkTypes() {	}

	private final float floatval;
}
/* End class floatLitNode */

/** Description of charLitNode class
 * Node for char literals
 */
class charLitNode extends exprNode {
	charLitNode(String val, int line, int col) {
		super(line,col,new Types(Types.Character), new Kinds(Kinds.Value));
		charval = val;
	}

	/** Description of checkTypes method
	 * Since charLitNode will always contain a character value,
	 * set in constructor
	 */
	void checkTypes() {	}
	private final String charval;
}
/* End class charLitNode */

/** Description of strLitNode class
 * Node for string literals
 */
class strLitNode extends exprNode {
	strLitNode(String stringval, int line, int col) {
		super(line,col,new Types(Types.String), new Kinds(Kinds.Value));
		length = stringval.length();
		strval = stringval;
	}

	/** Description of checkTypes method
	 * Since strLitNode will always contain a String value,
	 * set in constructor
	 */			
	void checkTypes() {	}
	public int length;
	private String strval;
} // class strLitNode 

/** Description of trueNode class
 * Node for true boolean literals
 */
class trueNode extends exprNode {
	trueNode(int line, int col) {
		super(line,col,new Types(Types.Boolean), new Kinds(Kinds.Value));
	}

	/** Description of checkTypes method
	 * Since trueNode will always contain a boolean value,
	 * set in constructor
	 */		
	void checkTypes() {	}
} 
/* End class trueNode */

/** Description of falseNode class
 * Node for false boolean literals
 */
class falseNode extends exprNode {
	falseNode(int line, int col) {
		super(line,col,new Types(Types.Boolean), new Kinds(Kinds.Value));
	}

	/** Description of checkTypes method
	 * Since falseNode will always contain a boolean value,
	 * set in constructor
	 */		
	void checkTypes() {	}
}
/* End class falseNode */
