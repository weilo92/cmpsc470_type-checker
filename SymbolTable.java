import java.io.*;
import java.util.*;

class SymbolTable {

	private ArrayList<Hashtable<String, Symb>> symbolTable;

	SymbolTable() {
		symbolTable = new ArrayList<Hashtable<String, Symb>>();
	}

	public void openScope() {
		symbolTable.add(new Hashtable<String, Symb>());
//		System.out.println("New scope opened.");
	}

	public void closeScope() throws EmptySTException {
		if (symbolTable.size() == 0)
			throw new EmptySTException();
		symbolTable.remove(symbolTable.size() - 1);
//		System.out.println("Top scope closed.");
//		System.out.println("No top scope exists in symbol table.");
	}

	public void insert(Symb s) throws DuplicateException, EmptySTException {
		if (symbolTable.size() == 0)
			throw new EmptySTException();
		if (localLookup(s.name()) != null)
			throw new DuplicateException();
		symbolTable.get(symbolTable.size() - 1).put(s.name().toLowerCase(), s);
		System.out.println(s.toString());
	}

	public Symb localLookup(String s) {
		Hashtable<String, Symb> localScope;
		Symb localSymbol = null;

		if (symbolTable.size() > 0) {
			localScope = symbolTable.get(symbolTable.size() - 1);

			if (localScope.containsKey(s.toLowerCase()))
				localSymbol = localScope.get(s.toLowerCase());
		}

		return localSymbol;
	}

	public Symb globalLookup(String s) {
		Hashtable<String, Symb> currentScope;
		Symb globalSymbol = null;
		int scope = symbolTable.size() - 1;

		if (scope > 0) {
			currentScope = symbolTable.get(scope);

			while (scope >= 0 && globalSymbol == null) {
				if (currentScope.containsKey(s.toLowerCase()))
					globalSymbol = currentScope.get(s.toLowerCase());
				
				--scope;

				if (scope >= 0)
					currentScope = symbolTable.get(scope);
			}
		}

		return globalSymbol;
	}

	public String toString() {
		StringBuilder output = new StringBuilder();
		Hashtable<String, Symb> currentScope;
		Set<String> currentSymbolNames;
		Iterator<String> symbolIter;
		int symbolCount;

		output.append("Contents of symbol table:");

		for (int scope = symbolTable.size() - 1; scope >= 0; --scope) {
			currentScope = symbolTable.get(scope);
			currentSymbolNames = currentScope.keySet();
			symbolIter = currentSymbolNames.iterator();

			symbolCount = 0;
			output.append(System.lineSeparator() + "{");

			while (symbolIter.hasNext()) {
				if (symbolCount > 0)
					output.append(", ");

				String symbolName = symbolIter.next().toString();
				output.append(symbolName + "=" + currentScope.get(symbolName).toString());
			}

			output.append("}");

		}

		return output.toString(); 
	}

	void dump(PrintStream ps) {
		Hashtable<String, Symb> currentScope;
		Set<String> currentSymbolNames;
		Iterator<String> symbolIter;
		int symbolCount;

		ps.print("Contents of symbol table:");

		for (int scope = symbolTable.size() - 1; scope >= 0; --scope) {
			currentScope = symbolTable.get(scope);
			currentSymbolNames = currentScope.keySet();
			symbolIter = currentSymbolNames.iterator();

			symbolCount = 0;
			ps.append(System.lineSeparator() + "{");

			while (symbolIter.hasNext()) {
				if (symbolCount > 0)
					ps.append(", ");

				String symbolName = symbolIter.next().toString();
				ps.append(symbolName + "=" + currentScope.get(symbolName).toString());
			}

			ps.append("}");

		}
	}
} // class SymbolTable\
