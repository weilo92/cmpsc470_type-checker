class test2 {
	void forWhileIf() {
		bool x;
		bool y = 1 <= 1;
		int i;
		while (true)
			i = 1;
		a: while (x)
			i = 1;
		a: while (y)
			i = 1;
		if (x)
			if (y)
				i = 2;
			endif
		endif
	}
	
	void readPrint() {
		int a;
		float b;
		char c;
		char ca[1];
		bool x;
		int arr[5];
		read(a, c);
		read(b);
		print(a, x, b, c, ca, "1");
	}
	
	void breakContinues() {
		int require;
		x: while (true) {
			int val;
			val = 1;
			break x;
			continue x;
		}
	}
	
	void asgTest() {
		int a;
		int arr[5];
		int b;
		char c;
		char carr[5];
		char carr2[5];
		char carr3[4];
		
		carr = "1234";
		carr = "12345";
		carr = carr2;
		carr = carr3;
		a = a;
	}
	void main() {
		int i;
		i = i;
	}
}