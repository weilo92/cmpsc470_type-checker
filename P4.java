import java.io.*;
import java_cup.runtime.*;

/** Description of P4 class
 * The P4 class contains the running logic for our compiler program.
 * A given input file is taken, opened, scanner, parsed, and then type checked
 * Relevant errors are given, and detailed output is given on the type
 * checked nodes, marking errors as they are found.
 */
class P4 {

	public static void
	main(String args[]) throws java.io.IOException,  Exception {

		if (args.length != 1) {
			System.out.println("Error: Input file must be named on command line." );
			System.exit(-1);
		}

		java.io.FileInputStream yyin = null;
		try {
			yyin = new java.io.FileInputStream(args[0]);
		} catch (FileNotFoundException notFound) {
			System.out.println ("Error: unable to open input file.");
			System.exit(-1);
		}

		Scanner.init(yyin); // Initialize Scanner class for parser
		final parser csxParser = new parser();
		System.out.println ("\n\n" + "Begin CSX compilation of " + args[0] + ".\n");
		Symbol root=null;
		try {
			root = csxParser.parse(); // do the parse
			System.out.println ("CSX program parsed correctly.");
		} catch (SyntaxErrorException e) {
			System.out.println ("Compilation terminated due to syntax errors.");
			System.exit(0);
		}

		// Start type checking on root note by invoking isTypeCorrect
		final boolean ok = ((classNode)root.value).isTypeCorrect();
		if (ok) {
			System.out.println("No CSX type errors detected.");
		} else {
			// Display error count
			System.out.println("\nCSX compilation halted due to "
				+ ((classNode)root.value).returnErrors() + " type errors.");
		}
	} // main
} // class P4
