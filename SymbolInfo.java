import java.util.*;

/** Description of SymbolInfo class
 * Builds up Symb class so it can contain type and kind information
 * LinkedLists are used to keep track of type/kind pairs for
 * any parameters that may need referenced later in type checking process.
 * SymbolInfo class instances will be pushed to the symbol table in 
 * the type checker program.
 */
class SymbolInfo extends Symb {
	public Kinds kind; // Should always be Var in CSX-lite
	public Types type; // Should always be Integer or Boolean in CSX-lite
	public LinkedList<Kinds> kindsParam;
	public LinkedList<Types> typesParam;
	public int arraySize;

	public SymbolInfo(String id, Kinds k, Types t){
		super(id);
		kind = k; 
		type = t;
	}
	public SymbolInfo(String id, int k, int t){
		super(id);
		kind = new Kinds(k); 
		type = new Types(t);
	}
	public SymbolInfo(String id, Kinds k, Types t, 
			LinkedList<Kinds> kP, LinkedList<Types> tP){
		super(id);
		kind = k; 
		type = t;
		kindsParam = kP;
		typesParam = tP;
	}
	public SymbolInfo(String id, int k, int t, 
						LinkedList<Kinds> kP, LinkedList<Types> tP){
		super(id);
		kind = new Kinds(k); 
		type = new Types(t);
		kindsParam = kP;
		typesParam = tP;
	}
	public SymbolInfo(String id, Kinds k, Types t, int as){
		super(id);
		kind = k; 
		type = t;
		arraySize = as;
	}
	public SymbolInfo(String id, int k, int t, int as){
		super(id);
		kind = new Kinds(k); 
		type = new Types(t);
		arraySize = as;
	}

	/** Description of toString method
	 * Output symb name, and type/kind
	 */	
	public String toString(){
		return "("+name()+": kind=" + kind+ ", type="+  type+")";
	}
}

